/*
 * Copyright (c) 2013 Cisco Systems, Inc. and others.  All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */

package org.opendaylight.controller.protocol_plugin.openflow13.internal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.opendaylight.controller.sal.action.Action;
import org.opendaylight.controller.sal.action.ActionType;
import org.opendaylight.controller.sal.action.Controller;
import org.opendaylight.controller.sal.action.Drop;
import org.opendaylight.controller.sal.action.Enqueue;
import org.opendaylight.controller.sal.action.Flood;
import org.opendaylight.controller.sal.action.FloodAll;
import org.opendaylight.controller.sal.action.HwPath;
import org.opendaylight.controller.sal.action.Loopback;
import org.opendaylight.controller.sal.action.Output;
import org.opendaylight.controller.sal.action.PopVlan;
import org.opendaylight.controller.sal.action.SetDlDst;
import org.opendaylight.controller.sal.action.SetDlSrc;
import org.opendaylight.controller.sal.action.SetNwDst;
import org.opendaylight.controller.sal.action.SetNwSrc;
import org.opendaylight.controller.sal.action.SetNwTos;
import org.opendaylight.controller.sal.action.SetTpDst;
import org.opendaylight.controller.sal.action.SetTpSrc;
import org.opendaylight.controller.sal.action.SetVlanId;
import org.opendaylight.controller.sal.action.SetVlanPcp;
import org.opendaylight.controller.sal.action.SwPath;
import org.opendaylight.controller.sal.utils.EtherTypes;
import org.opendaylight.controller.sal.utils.IPProtocols;
import org.opendaylight.controller.sal.utils.NetUtils;
import org.opendaylight.controller.sal.core.Node;
import org.opendaylight.controller.sal.core.NodeConnector;
import org.opendaylight.controller.sal.groupprogrammer.Group;
import org.opendaylight.controller.sal.groupprogrammer.Bucket;
import org.opendaylight.controller.sal.match.Match;
import org.opendaylight.controller.sal.match.MatchType;
import org.opendaylight.controller.sal.match.MatchField;
import org.openflow.protocol.OFGroupMod;
import org.openflow.protocol.OFMatch;
import org.openflow.protocol.OFMessage;
import org.openflow.protocol.OFOXMField;
import org.openflow.protocol.OFOXMFieldType;
import org.openflow.protocol.OFPacketOut;
import org.openflow.protocol.OFPort;
import org.openflow.protocol.OFQueue;
import org.openflow.protocol.OFVlanId;
import org.openflow.protocol.action.OFAction;
import org.openflow.protocol.action.OFActionPopVLAN;
import org.openflow.protocol.action.OFActionSetField;
import org.openflow.protocol.action.OFActionOutput;
import org.openflow.protocol.action.OFActionSetQueue;
import org.openflow.protocol.instruction.OFInstruction;
import org.openflow.protocol.instruction.OFInstructionApplyActions;
import org.openflow.protocol.instruction.OFInstructionGotoTable;
import org.openflow.protocol.OFBucket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class for converting a SAL Group into the OF Group and vice-versa
 */
public class GroupConverter {
    protected static final Logger logger = LoggerFactory
            .getLogger(GroupConverter.class);

    private Group group; // SAL Group
    private List<OFBucket> bucketsList; // OF 1.3 buckets

    public GroupConverter( List<OFBucket> bucketsList) {
        this.bucketsList = bucketsList;
        this.group = null;
    }

    public GroupConverter(Group group) {
        this.bucketsList = null;
        this.group = group;
    }

    /**
     * Returns the list of buckets
     *
     * @return
     */
    public List<OFBucket> getOFBuckets() {
        logger.trace("getOFBuckets bucketList: {}", this.bucketsList);
        if (this.bucketsList == null) {
            bucketsList = new ArrayList<OFBucket>();
            for (Bucket bucket : group.getBuckets()) 
            {
                OFBucket ofb = new OFBucket();

                //Read the SAL actions and set OFActions
                List<OFAction> ofActionList = getOFActions(bucket);
                ofb.setActions(ofActionList);
                ofb.setWatchGroup(bucket.getWatchGroup());
                ofb.setWatchPort(bucket.getWatchPort());
                ofb.setWeight(bucket.getWeight());

                bucketsList.add(ofb);
            }
        }
        return bucketsList;
    }


    /**
     * Returns the list of actions in OF 1.0 form
     *
     * @return
     */
    public List<OFAction> getOFActions( Bucket b ) {
        List<OFAction> actionsList = new ArrayList<OFAction>();
            for (Action action : b.getActions()) {
                if (action.getType() == ActionType.OUTPUT) {
                    Output a = (Output) action;
                    OFActionOutput ofAction = new OFActionOutput();
                    ofAction.setMaxLength((short) 0xffff);
                    ofAction.setPort(PortConverter.toOFPort(a.getPort()));
                    actionsList.add(ofAction);
                    continue;
                }

                if (action.getType() == ActionType.ENQUEUE) {
                    Enqueue a = (Enqueue) action;
                    OFActionSetQueue ofAction = new OFActionSetQueue();
                    ofAction.setQueueId(a.getQueue());
                    actionsList.add(ofAction);
                    continue;
                }

                if (action.getType() == ActionType.DROP) {
                    continue;
                }
                if (action.getType() == ActionType.LOOPBACK) {
                    OFActionOutput ofAction = new OFActionOutput();
                    ofAction.setPort(OFPort.OFPP_IN_PORT.getValue());
                    actionsList.add(ofAction);
                    continue;
                }
                if (action.getType() == ActionType.FLOOD) {
                    OFActionOutput ofAction = new OFActionOutput();
                    ofAction.setPort(OFPort.OFPP_FLOOD.getValue());
                    actionsList.add(ofAction);
                    continue;
                }
                if (action.getType() == ActionType.FLOOD_ALL) {
                    OFActionOutput ofAction = new OFActionOutput();
                    ofAction.setPort(OFPort.OFPP_ALL.getValue());
                    actionsList.add(ofAction);
                    continue;
                }
                if (action.getType() == ActionType.CONTROLLER) {
                    OFActionOutput ofAction = new OFActionOutput();
                    ofAction.setPort(OFPort.OFPP_CONTROLLER.getValue());
                    // We want the whole frame hitting the match be sent to the
                    // controller
                    ofAction.setMaxLength((short) 0xffff);
                    actionsList.add(ofAction);
                    continue;
                }
                if (action.getType() == ActionType.SW_PATH) {
                    OFActionOutput ofAction = new OFActionOutput();
                    ofAction.setPort(OFPort.OFPP_LOCAL.getValue());
                    actionsList.add(ofAction);
                    continue;
                }
                if (action.getType() == ActionType.HW_PATH) {
                    OFActionOutput ofAction = new OFActionOutput();
                    ofAction.setPort(OFPort.OFPP_NORMAL.getValue());
                    actionsList.add(ofAction);
                    continue;
                }
                if (action.getType() == ActionType.SET_VLAN_ID) {
                    SetVlanId a = (SetVlanId) action;
                    OFActionSetField ofAction = new OFActionSetField(OFOXMFieldType.VLAN_VID, a.getVlanId());
                    actionsList.add(ofAction);
                    continue;
                }
                if (action.getType() == ActionType.SET_VLAN_PCP) {
                    SetVlanPcp a = (SetVlanPcp) action;
                    OFActionSetField ofAction = new OFActionSetField(OFOXMFieldType.VLAN_PCP, Integer.valueOf(
                            a.getPcp()).byteValue());
                    actionsList.add(ofAction);
                    continue;
                }
                if (action.getType() == ActionType.POP_VLAN) {
                    OFActionPopVLAN ofAction = new OFActionPopVLAN();
                    actionsList.add(ofAction);
                    continue;
                }
                if (action.getType() == ActionType.SET_DL_SRC) {
                    SetDlSrc a = (SetDlSrc) action;
                    OFActionSetField ofAction = new OFActionSetField(OFOXMFieldType.ETH_SRC, a.getDlAddress());
                    actionsList.add(ofAction);
                    continue;
                }
                if (action.getType() == ActionType.SET_DL_DST) {
                    SetDlDst a = (SetDlDst) action;
                    OFActionSetField ofAction = new OFActionSetField(OFOXMFieldType.ETH_DST, a.getDlAddress());
                    actionsList.add(ofAction);
                    continue;
                }
                if (action.getType() == ActionType.SET_NW_SRC) {
                    SetNwSrc a = (SetNwSrc) action;
                    OFActionSetField ofAction = new OFActionSetField(OFOXMFieldType.IPV4_SRC, a.getAddressAsString());
                    actionsList.add(ofAction);
                    continue;
                }
                if (action.getType() == ActionType.SET_NW_DST) {
                    SetNwDst a = (SetNwDst) action;
                    OFActionSetField ofAction = new OFActionSetField(OFOXMFieldType.IPV4_DST, a.getAddressAsString());
                    actionsList.add(ofAction);

                    continue;
                }
                if (action.getType() == ActionType.SET_NW_TOS) {
                    SetNwTos a = (SetNwTos) action;
                    OFActionSetField ofAction = new OFActionSetField(OFOXMFieldType.IP_DSCP, Integer.valueOf(
                            a.getNwTos()).byteValue());
                    actionsList.add(ofAction);
                    continue;
                }
                if (action.getType() == ActionType.SET_TP_SRC) {
                    SetTpSrc a = (SetTpSrc) action;
                    OFActionSetField ofAction = new OFActionSetField(OFOXMFieldType.TCP_SRC, Integer.valueOf(a.getPort())
                            .shortValue());
                    actionsList.add(ofAction);
                    continue;
                }
                if (action.getType() == ActionType.SET_TP_DST) {
                    SetTpDst a = (SetTpDst) action;
                    OFActionSetField ofAction = new OFActionSetField(OFOXMFieldType.TCP_DST, Integer.valueOf(a.getPort())
                            .shortValue());
                    actionsList.add(ofAction);
                    continue;
                }
                if (action.getType() == ActionType.SET_NEXT_HOP) {
                    logger.warn("Unsupported action: {}", action);
                    continue;
                }
            }
        //}
        logger.trace("SAL Bucket Actions: {} Openflow Actions: {}", b.getActions(),
                actionsList);
        return actionsList;
    }


    /**
     * Utility to convert a SAL group to an OF 1.1 (OFGroupMod) 
     *
     * @param sw
     * @param command
     * @return
     */
    public OFMessage getOFGroupMod(short command) {
        OFMessage fm = new OFGroupMod();
        logger.trace("getOFGroupMod command: {}", command);
        if (this.bucketsList == null) {
            getOFBuckets();
        }

        if ((command != OFGroupMod.OFPGC_DELETE)) 
        {
            ((OFGroupMod) fm).setGroupId((int)group.getId());
            ((OFGroupMod) fm).setGroupType(group.getType());
            ((OFGroupMod) fm).setBuckets(this.bucketsList);
            ((OFGroupMod) fm).setCommand(command);
        }
        logger.trace("Openflow Buckets: {}", bucketsList);
        logger.trace("Openflow Mod Message: {}", fm);
        return fm;
    }


    private static final Map<Integer, Class<? extends Action>> actionMap = new HashMap<Integer, Class<? extends Action>>() {
        private static final long serialVersionUID = 1L;
        {
            put(1 << 0, Output.class);
            put(1 << 1, SetVlanId.class);
            put(1 << 2, SetVlanPcp.class);
            put(1 << 3, PopVlan.class);
            put(1 << 4, SetDlSrc.class);
            put(1 << 5, SetDlDst.class);
            put(1 << 6, SetNwSrc.class);
            put(1 << 7, SetNwDst.class);
            put(1 << 8, SetNwTos.class);
            put(1 << 9, SetTpSrc.class);
            put(1 << 10, SetTpDst.class);
            put(1 << 11, Enqueue.class);
        }
    };

    /**
     * Returns the supported flow actions for the netwrok node given the bitmask
     * representing the actions the Openflow 1.0 switch supports
     *
     * @param ofActionBitmask
     *            OF 1.0 action bitmask
     * @return The correspondent list of SAL Action classes
     */
    public static List<Class<? extends Action>> getGroupActions(int ofActionBitmask) {
        List<Class<? extends Action>> list = new ArrayList<Class<? extends Action>>();

        for (int i = 0; i < Integer.SIZE; i++) {
            int index = 1 << i;
            if ((index & ofActionBitmask) > 0) {
                if (actionMap.containsKey(index)) {
                    list.add(actionMap.get(index));
                }
            }
        }
        // Add implicit SAL actions
        list.add(Controller.class);
        list.add(SwPath.class);
        list.add(HwPath.class);
        list.add(Drop.class);
        list.add(Flood.class);
        list.add(FloodAll.class);

        return list;
    }

}
