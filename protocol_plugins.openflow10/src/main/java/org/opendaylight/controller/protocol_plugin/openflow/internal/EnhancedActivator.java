
package org.opendaylight.controller.protocol_plugin.openflow.internal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.felix.dm.Component;
import org.opendaylight.controller.protocol_plugin.openflow.core.IController;
import org.opendaylight.controller.protocol_plugin.openflow.core.internal.Controller;
import org.opendaylight.controller.protocol_plugin.openflow.core.internal.EnhancedController;
import org.opendaylight.controller.sal.connection.IPluginInConnectionService;
import org.sdnhub.odl.ofbroker.IOFPlugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EnhancedActivator extends Activator {
    private static final Logger logger = LoggerFactory
            .getLogger(EnhancedActivator.class);

    /**
     * Function that is used to communicate to dependency manager the list of
     * known implementations for services that are container independent.
     *
     *
     * @return An array containing all the CLASS objects that will be
     *         instantiated in order to get an fully working implementation
     *         Object
     */
    @Override
    public Object[] getGlobalImplementations() {
        Object[] res = super.getGlobalImplementations();
        // Now remove the Controller.class and return the
        // EnhancedController
        List resList = new ArrayList(Arrays.asList(res));
        resList.remove(Controller.class);
        resList.add(EnhancedController.class);
        return resList.toArray();
    }

    /**
     * Function that is called when configuration of the dependencies is
     * required.
     *
     * @param c
     *            dependency manager Component object, used for configuring the
     *            dependencies exported and imported
     * @param imp
     *            Implementation class that is being configured, needed as long
     *            as the same routine can configure multiple implementations
     */
    @Override
    public void configureGlobalInstance(Component c, Object imp) {
        if (imp.equals(EnhancedController.class)) {
            super.configureGlobalInstance(c, Controller.class);
            /*
             * Hate to do this. But Felix Dependency manager has no access method to get the Interface
             * set on the Component. Hence overriding the Interfaces with the additional IOFPlugin as well.
             * In the future, care must be taken to ensure all the parent services are replicated here as well.
             */
            c.setInterface(new String[] { IController.class.getName(), IOFPlugin.class.getName(),
                                          IPluginInConnectionService.class.getName()},
                                          c.getServiceProperties());
        } else {
            super.configureGlobalInstance(c, imp);
        }
    }
}
