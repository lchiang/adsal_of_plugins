This is an attempt to 
1. Write an OpenDaylight AD-SAL based Openflow13 plugin implementation using the JOpenFlow (https://bitbucket.org/sdnhub/jopenflow) library 
2. Write an enhanced Openflow10 plugin based on existing OpenDaylight AD-SAL based Openflow10 plugin with additional support of OFBroker.
contributed by Srini & Madhu from SDNHub.

Build Instructions
------------------
Pre-Requisites
--------------

1. JDK 1.7+
2. Maven 3.0+
3. Internet connection (to download artifacts from the ODL & SDNHub nexus repos)

Execute the following command :

mvn clean install -Dmaven.test.skip=true.

